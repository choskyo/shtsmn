#!/bin/sh

go mod vendor
docker build -t shitsumon-api .
docker rm --force shitsumon
docker run -d -h api -p 8080:8080 --name shitsumon --network shitsumon shitsumon-api:latest
docker start shitsumon