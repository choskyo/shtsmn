package repo

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Room ...
type Room struct {
	ID        uuid.UUID `gorm:"type:uuid;primary_key;default:gen_random_uuid()"`
	Creator   User
	CreatorID uuid.UUID
	Name      string `gorm:"type:varchar(16);not null"`
	Password  string
	Ended     bool
	CreatedAt time.Time
	UpdatedAt time.Time
}
