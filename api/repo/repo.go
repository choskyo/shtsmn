package repo

import (
	"os"

	"github.com/jinzhu/gorm"
	// DB dialect is a required import
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// GetDB returns db connection
func GetDB() *gorm.DB {
	db, err := gorm.Open("postgres", os.Getenv("DB_CONSTR"))

	if err != nil {
		panic(err)
	}

	return db
}
