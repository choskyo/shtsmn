package repo

import uuid "github.com/satori/go.uuid"

// Question ...
type Question struct {
	ID       uuid.UUID `json:"id"`
	Category string    `json:"category"`
	Text     string    `json:"text"`
}

// PlaceholderQuestions until real ones added
var PlaceholderQuestions = []Question{}
