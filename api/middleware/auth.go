package middleware

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/gin-gonic/gin"
)

// JWTPayload custom type containing JWT fields
type JWTPayload struct {
	jwt.Payload
	SubjectName string `json:"subName,omitempty"`
}

// Auth sets userID value in gin context or aborts unauthorised requests
func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		v := strings.Split(header, " ")

		if len(v) < 2 {
			c.JSON(http.StatusUnauthorized, gin.H{"err": "no bearer token found"})
			c.Abort()
			return
		}

		token := v[1]

		var claims JWTPayload

		now := time.Now()
		// aud := jwt.Audience{"http://localhost:3000"}

		iatValidator := jwt.IssuedAtValidator(now)
		expValidator := jwt.ExpirationTimeValidator(now)
		// audValidator := jwt.AudienceValidator(aud)

		validationOpts := jwt.ValidatePayload(&claims.Payload, iatValidator, expValidator)
		_, err := jwt.Verify([]byte(token), jwt.NewHS256([]byte(os.Getenv("AUTH_SIGNING_KEY"))), &claims, validationOpts)

		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"err": err.Error()})
			c.Abort()
			return
		}

		c.Set("userID", claims.Subject)

		c.Next()
	}
}
