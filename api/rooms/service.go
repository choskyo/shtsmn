package rooms

import (
	"errors"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/repo"
	"gitlab.com/choskyo/shitsumon-api/ws"
	"golang.org/x/crypto/bcrypt"
)

// RoomService ...
type RoomService interface {
	ListOpenRooms() ([]ws.RoomCacheModel, error)
	GetDetails(id uuid.UUID) (ws.RoomCacheModel, error)
	Create(roomCreateModel) error
	JoinGame(roomID uuid.UUID, userID uuid.UUID, password string) error
	Close(uuid.UUID) error
}

type roomService struct {
	db *gorm.DB
}

func (s *roomService) ListOpenRooms() ([]ws.RoomCacheModel, error) {
	cache := ws.GetCache()
	vms := []ws.RoomCacheModel{}

	for _, v := range cache {
		vms = append(vms, *v)
	}

	return vms, nil
}

func (s *roomService) GetDetails(roomID uuid.UUID) (ws.RoomCacheModel, error) {
	if room := ws.GetOpenRoom(roomID); room != nil {
		return *room, nil
	}

	return ws.RoomCacheModel{}, errors.New("Room not found")
}

func (s *roomService) Create(r roomCreateModel) error {
	hashedPassword := ""

	if len(r.Password) > 0 {
		hashedBytes, _ := bcrypt.GenerateFromPassword([]byte(r.Password), 4)
		hashedPassword = string(hashedBytes)
	}

	newRoom := &repo.Room{
		Name:      r.Name,
		CreatorID: r.CreatorID,
		Password:  hashedPassword,
	}

	if err := s.db.Create(newRoom).Error; err != nil {
		return err
	}

	vm := newViewModel(*newRoom)

	ws.RegisterRoomCreated(vm)

	return nil
}

func (s *roomService) JoinGame(roomID uuid.UUID, userID uuid.UUID, password string) error {
	cache := ws.GetCache()
	room := cache[roomID]

	if room.HasPassword {
		if err := bcrypt.CompareHashAndPassword([]byte(room.Password), []byte(password)); err != nil {
			return err
		}
	}

	alreadyInRoom := false
	for _, p := range room.Players {
		if p == userID {
			alreadyInRoom = true
			break
		}
	}

	if !alreadyInRoom {
		room.Players = append(room.Players, userID)
		cache[roomID] = room
	}

	ws.RegisterJoinRoom(userID, roomID)

	return nil
}

// TODO: Fix
func (s *roomService) Close(id uuid.UUID) error {
	if err := s.db.Model(&repo.Room{ID: id}).Update("ended", true).Error; err != nil {
		return err
	}

	ws.RegisterRoomClosed(id)

	return nil
}
