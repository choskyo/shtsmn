package rooms

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/repo"
	"gitlab.com/choskyo/shitsumon-api/ws"
)

type roomCreateModel struct {
	Name      string    `json:"name"`
	Password  string    `json:"password"`
	CreatorID uuid.UUID `json:"creatorId"`
}

func newViewModel(r repo.Room) ws.RoomCacheModel {
	return ws.RoomCacheModel{
		ID:          r.ID,
		Name:        r.Name,
		CreatorID:   r.CreatorID,
		Players:     []uuid.UUID{},
		Password:    r.Password,
		HasPassword: len(r.Password) > 0,
	}
}

type joinGameRequestModel struct {
	ID       uuid.UUID `json:"id"`
	Password string    `json:"password"`
}
