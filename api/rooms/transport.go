package rooms

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gitlab.com/choskyo/shitsumon-api/repo"
)

// HandleEndpoints registers service's endpoints on router
func HandleEndpoints(r *gin.Engine) {
	db := repo.GetDB()

	db.AutoMigrate(&repo.Room{})

	svc := &roomService{
		db: db,
	}

	authRoutes := r.Group("/rooms/v1").Use(middleware.Auth())
	{
		authRoutes.GET("/list", makeListOpenRoomsEndpoint(svc))
		authRoutes.GET("/details/:id", makeGetDetailsEndpoint(svc))
		authRoutes.POST("/join/:id", makeJoinGameEndpoint(svc))
		authRoutes.POST("/create", makeCreateEndpoint(svc))
		authRoutes.GET("/close/:id", makeCloseEndpoint(svc))
	}
}

func makeGetDetailsEndpoint(svc RoomService) gin.HandlerFunc {
	return func(c *gin.Context) {
		roomID, err := uuid.FromString(c.Param("id"))

		if err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}

		room, err := svc.GetDetails(roomID)

		if err != nil {
			c.JSON(http.StatusInternalServerError, err.Error())
			return
		}

		c.JSON(http.StatusOK, gin.H{"room": room})
	}
}

func makeJoinGameEndpoint(svc RoomService) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID, _ := uuid.FromString(c.MustGet("userID").(string))

		var m joinGameRequestModel

		if err := c.ShouldBindJSON(&m); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		if err := svc.JoinGame(m.ID, userID, m.Password); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.Status(http.StatusOK)
	}
}

func makeListOpenRoomsEndpoint(svc RoomService) gin.HandlerFunc {
	return func(c *gin.Context) {
		rooms, err := svc.ListOpenRooms()

		if err != nil {
			c.JSON(http.StatusInternalServerError, err.Error())
			return
		}

		c.JSON(http.StatusOK, rooms)
	}
}

func makeCreateEndpoint(svc RoomService) gin.HandlerFunc {
	return func(c *gin.Context) {
		var cm roomCreateModel
		c.BindJSON(&cm)

		userID, _ := uuid.FromString(c.MustGet("userID").(string))

		cm.CreatorID = userID

		if err := svc.Create(cm); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.Status(http.StatusOK)
	}
}

func makeCloseEndpoint(svc RoomService) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := uuid.FromStringOrNil(c.Param("id"))

		room, _ := svc.GetDetails(id)
		userID := uuid.FromStringOrNil(c.MustGet("userID").(string))

		if room.CreatorID != userID {
			c.JSON(http.StatusBadRequest, gin.H{"err": "Only the room's creator can close it."})
			return
		}

		if err := svc.Close(id); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.Status(http.StatusOK)
	}
}
