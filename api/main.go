package main

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/choskyo/shitsumon-api/rooms"
	"gitlab.com/choskyo/shitsumon-api/users"
	"gitlab.com/choskyo/shitsumon-api/ws"
)

func main() {
	router := gin.Default()
	c := cors.DefaultConfig()
	c.AddAllowHeaders("Authorization")
	c.AllowOrigins = []string{"*", "http://localhost:3000"}
	router.Use(cors.New(c))

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"err": "page not found"})
		return
	})

	ws.Initialise(router)

	users.HandleEndpoints(router)
	rooms.HandleEndpoints(router)

	router.Run(":8080")
}
