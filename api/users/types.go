package users

import (
	uuid "github.com/satori/go.uuid"
)

type createUserModel struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type userViewModel struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}
