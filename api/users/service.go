package users

import (
	"os"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gitlab.com/choskyo/shitsumon-api/repo"
	"golang.org/x/crypto/bcrypt"
)

// UserService ...
type UserService interface {
	Register(createUserModel) (uuid.UUID, error)
	FetchByID(uuid.UUID) (userViewModel, error)
	Login(createUserModel) (string, error)
	RenewLogin(uuid.UUID) (string, error)
}

type userService struct {
	db *gorm.DB
}

func (s *userService) Register(m createUserModel) (uuid.UUID, error) {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(m.Password), 4)
	if err != nil {
		return uuid.Nil, err
	}

	newUser := repo.User{
		Name:     m.Name,
		Password: string(passwordHash),
	}

	if err := s.db.Create(&newUser).Error; err != nil {
		return uuid.Nil, err
	}

	return newUser.ID, nil
}

func (s *userService) FetchByID(id uuid.UUID) (userViewModel, error) {
	var user repo.User

	if err := s.db.First(&user, &repo.User{ID: id}).Error; err != nil {
		return userViewModel{}, err
	}

	vm := userViewModel{ID: user.ID, Name: user.Name}

	return vm, nil
}

func (s *userService) Login(m createUserModel) (string, error) {
	var user repo.User

	if err := s.db.First(&user, &repo.User{Name: m.Name}).Error; err != nil {
		return "", err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(m.Password)); err != nil {
		return "", err
	}

	now := time.Now()

	payload := &middleware.JWTPayload{
		SubjectName: user.Name,
		Payload: jwt.Payload{
			Issuer:         "quest-api",
			Audience:       jwt.Audience{os.Getenv("JWT_AUDIENCE")},
			IssuedAt:       jwt.NumericDate(now),
			ExpirationTime: jwt.NumericDate(now.Add(60 * time.Minute)),
			Subject:        user.ID.String(),
		},
	}

	tkn, err := jwt.Sign(payload, jwt.NewHS256([]byte(os.Getenv("AUTH_SIGNING_KEY"))))
	if err != nil {
		return "", err
	}

	return string(tkn), nil
}

func (s *userService) RenewLogin(userID uuid.UUID) (string, error) {
	var user repo.User

	if err := s.db.First(&user, &repo.User{ID: userID}).Error; err != nil {
		return "", err
	}

	now := time.Now()

	payload := &middleware.JWTPayload{
		SubjectName: user.Name,
		Payload: jwt.Payload{
			Issuer:         "quest-api",
			Audience:       jwt.Audience{"http://localhost:3000"},
			IssuedAt:       jwt.NumericDate(now),
			ExpirationTime: jwt.NumericDate(now.Add(60 * time.Minute)),
			Subject:        user.ID.String(),
		},
	}

	tkn, err := jwt.Sign(payload, jwt.NewHS256([]byte(os.Getenv("AUTH_SIGNING_KEY"))))
	if err != nil {
		return "", err
	}

	return string(tkn), nil
}
