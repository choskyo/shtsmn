package users

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gitlab.com/choskyo/shitsumon-api/repo"
)

// HandleEndpoints registers service's endpoints on router
func HandleEndpoints(r *gin.Engine) {
	db := repo.GetDB()

	db.AutoMigrate(&repo.User{})

	svc := &userService{
		db: db,
	}

	authRoutes := r.Group("/users/v1").Use(middleware.Auth())
	{
		authRoutes.GET("/fetch/:id", makeFetchEndpoint(svc))
		authRoutes.GET("/renew", makeRenewLoginEndpoint(svc))
	}

	r.POST("/users/v1/login", makeLoginEndpoint(svc))
	r.POST("/users/v1/register", makeRegisterEndpoint(svc))
}

func makeRegisterEndpoint(svc UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		var cm createUserModel

		if err := c.ShouldBind(&cm); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		newUserID, err := svc.Register(cm)

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"id": newUserID})
	}
}

func makeLoginEndpoint(svc UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		var cm createUserModel

		if err := c.ShouldBind(&cm); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err})
			return
		}

		token, err := svc.Login(cm)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
		}

		c.JSON(http.StatusOK, gin.H{
			"token": token,
		})
	}
}

func makeFetchEndpoint(svc UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")

		if len(id) == 0 {
			c.JSON(http.StatusBadRequest, gin.H{"err": "no ID given"})
			return
		}

		user, err := svc.FetchByID(uuid.FromStringOrNil(id))

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"user": user})
	}
}

func makeRenewLoginEndpoint(svc UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := uuid.FromStringOrNil(c.MustGet("userID").(string))

		token, err := svc.RenewLogin(userID)

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"token": token})
	}
}
