module gitlab.com/choskyo/shitsumon-api

go 1.13

require (
	github.com/gbrlsnchs/jwt/v3 v3.0.0-rc.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.5.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	gopkg.in/olahol/melody.v1 v1.0.0-20170518105555-d52139073376
)
