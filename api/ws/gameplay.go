package ws

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gopkg.in/olahol/melody.v1"
)

func handleToggleReady(s *melody.Session, claims middleware.JWTPayload) {
	roomID := sessionCache[s].roomID
	setPlayerReady := true
	playerIndex := 0

	fmt.Println(roomID)

	if len(gameCache[roomID].ReadyPlayers) > 0 {
		for idx, p := range gameCache[roomID].ReadyPlayers {
			if p.String() == claims.Subject {
				setPlayerReady = false
				playerIndex = idx
				break
			}
		}
	}

	if setPlayerReady {
		gameCache[roomID].ReadyPlayers = append(gameCache[roomID].ReadyPlayers, uuid.FromStringOrNil(claims.Subject))
	} else {
		if len(gameCache[roomID].ReadyPlayers) == 1 {
			gameCache[roomID].ReadyPlayers = make([]uuid.UUID, 0)
		} else {
			gameCache[roomID].ReadyPlayers[playerIndex] = gameCache[roomID].ReadyPlayers[len(gameCache[roomID].ReadyPlayers)-1]
			gameCache[roomID].ReadyPlayers = gameCache[roomID].ReadyPlayers[:len(gameCache[roomID].ReadyPlayers)-1]
		}
	}

	game := gameCache[sessionCache[s].roomID]

	// All players are ready
	if len(game.ReadyPlayers) == len(game.Players) {
		gameCache[sessionCache[s].roomID].Questions = append(gameCache[sessionCache[s].roomID].Questions, getNewQuestion(roomID))

		gameCache[sessionCache[s].roomID].ReadyPlayers = make([]uuid.UUID, 0)

		advanceTurn(roomID)
	}

	room, _ := json.Marshal(gameCache[sessionCache[s].roomID])

	mdy.BroadcastFilter([]byte("gameUpdate:"+string(room)), func(q *melody.Session) bool {
		return sessionCache[q].roomID == sessionCache[s].roomID
	})
}
