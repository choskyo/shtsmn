package ws

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/repo"
)

type chatMessage struct {
	Time       string `json:"time"`
	SenderID   string `json:"senderId"`
	SenderName string `json:"senderName"`
	Content    string `json:"content"`
}

type urPair struct {
	userID uuid.UUID
	roomID uuid.UUID
}

// RoomCacheModel used to store room data while the game is in-process
type RoomCacheModel struct {
	ID           uuid.UUID         `json:"id"`
	Name         string            `json:"name"`
	CreatorID    uuid.UUID         `json:"creatorId"`
	ReadyPlayers []uuid.UUID       `json:"readyPlayers"`
	Players      []uuid.UUID       `json:"players"`
	Turn         uuid.UUID         `json:"turn"`
	Questions    []repo.Question   `json:"questions"`
	HasPassword  bool              `json:"hasPassword"`
	Password     string            `json:"-"`
	Messages     []chatMessage     `json:"messages"`
	GoodAnswers  map[uuid.UUID]int `json:"goodAnswers"`
	BadAnswers   map[uuid.UUID]int `json:"badAnswers"`
}
