package ws

import (
	"encoding/json"
	"time"

	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gopkg.in/olahol/melody.v1"
)

func handleChat(s *melody.Session, claims middleware.JWTPayload, messageContent string) {
	res := chatMessage{
		Time:       time.Now().String(),
		SenderID:   claims.Subject,
		Content:    messageContent,
		SenderName: claims.SubjectName,
	}

	resJSON, err := json.Marshal(res)
	if err != nil {
		s.Write([]byte("err:" + err.Error()))
		return
	}

	mdy.BroadcastFilter([]byte("chat:"+string(resJSON)), func(q *melody.Session) bool {
		return sessionCache[q].roomID == sessionCache[s].roomID
	})
}
