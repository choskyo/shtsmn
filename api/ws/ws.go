package ws

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/choskyo/shitsumon-api/middleware"
	"gitlab.com/choskyo/shitsumon-api/repo"
	"gopkg.in/olahol/melody.v1"
)

var mdy *melody.Melody

var sessionCache map[*melody.Session]urPair

// maps RoomID:Model
var gameCache map[uuid.UUID]*RoomCacheModel

// Initialise loads mdy instance + listens on routes
func Initialise(router *gin.Engine) {
	mdy = melody.New()
	sessionCache = make(map[*melody.Session]urPair)
	gameCache = make(map[uuid.UUID]*RoomCacheModel)

	router.GET("/ws", func(c *gin.Context) {
		mdy.HandleRequest(c.Writer, c.Request)
	})

	mdy.HandleDisconnect(func(s *melody.Session) {
		delete(sessionCache, s)
	})

	mdy.HandleMessage(handleWs)
}

func handleWs(s *melody.Session, rawMessage []byte) {
	message := string(rawMessage)
	messageParts := strings.SplitN(message, ":", 3)
	messageType, authTokenString, messageContent := messageParts[0], messageParts[1], strings.Trim(messageParts[2], " ")

	var claims middleware.JWTPayload

	now := time.Now()
	// aud := jwt.Audience{"http://localhost:3000"}

	iatValidator := jwt.IssuedAtValidator(now)
	expValidator := jwt.ExpirationTimeValidator(now)
	// audValidator := jwt.AudienceValidator(aud)

	validationOpts := jwt.ValidatePayload(&claims.Payload, iatValidator, expValidator)
	_, err := jwt.Verify([]byte(authTokenString), jwt.NewHS256([]byte(os.Getenv("AUTH_SIGNING_KEY"))), &claims, validationOpts)

	if err != nil {
		// TODO: return "err:errormessage" packet
		return
	}

	switch messageType {
	case "init":
		{
			sessionCache[s] = urPair{userID: uuid.FromStringOrNil(claims.Subject), roomID: uuid.Nil}
		}
	case "chat":
		{
			handleChat(s, claims, messageContent)
		}
	case "toggleReady":
		{
			handleToggleReady(s, claims)
		}
	}
}

// RegisterJoinRoom call once user has been added to cache in RoomService
// to ensure chat messages are received
func RegisterJoinRoom(userID uuid.UUID, roomID uuid.UUID) {
	var session *melody.Session

	fmt.Println("sessionCache")
	fmt.Println(sessionCache)

	for s, p := range sessionCache {
		if p.userID == userID {
			sessionCache[s] = urPair{userID: userID, roomID: roomID}
			session = s
			break
		}
	}

	room, _ := json.Marshal(gameCache[sessionCache[session].roomID])

	mdy.BroadcastFilter([]byte("gameUpdate:"+string((room))), func(q *melody.Session) bool {
		fmt.Println("sending to: " + sessionCache[q].userID.String())
		return sessionCache[q].roomID == sessionCache[session].roomID
	})

	res := chatMessage{
		Time:       time.Now().String(),
		SenderID:   uuid.Nil.String(),
		Content:    "Player has joined the game.",
		SenderName: "Server",
	}

	resJSON, err := json.Marshal(res)
	if err != nil {
		fmt.Println("ws err")
		fmt.Println(err.Error())
		return
	}

	mdy.BroadcastFilter([]byte("chat:"+string(resJSON)), func(q *melody.Session) bool {
		return sessionCache[q].roomID == sessionCache[session].roomID
	})
}

// RegisterRoomCreated called after rooms are added to the DB to prevent future lookups
// until archiving is added probably
func RegisterRoomCreated(r RoomCacheModel) {
	gameCache[r.ID] = &r
	gameCache[r.ID].Questions = append(gameCache[r.ID].Questions, getNewQuestion(r.ID))
	gameCache[r.ID].Turn = gameCache[r.ID].CreatorID
	gameCache[r.ID].BadAnswers = make(map[uuid.UUID]int)
	gameCache[r.ID].GoodAnswers = make(map[uuid.UUID]int)
	gameCache[r.ID].ReadyPlayers = make([]uuid.UUID, 0)
}

// RegisterRoomClosed should be called to end a game, prevent it showing in games list etc
func RegisterRoomClosed(roomID uuid.UUID) {
	delete(gameCache, roomID)

	mdy.BroadcastFilter([]byte("roomClosed:blablabla"), func(q *melody.Session) bool {
		return sessionCache[q].roomID == roomID
	})
}

// GetCache returns the in-progress gameCache
func GetCache() map[uuid.UUID]*RoomCacheModel {
	return gameCache
}

// GetOpenRoom returns cached room or nil if not found
func GetOpenRoom(roomID uuid.UUID) *RoomCacheModel {
	var room *RoomCacheModel

	if r := gameCache[roomID]; r != nil {
		room = r
	}

	return room
}

func getNewQuestion(roomID uuid.UUID) repo.Question {
	rand.Seed(time.Now().UnixNano())

	var newQuestion repo.Question

	for {
		hasQ := false
		newQuestion = repo.PlaceholderQuestions[rand.Intn(len(repo.PlaceholderQuestions))]

		for _, q := range gameCache[roomID].Questions {
			if q.ID == newQuestion.ID {
				hasQ = true
				break
			}
		}

		if !hasQ {
			break
		}
	}

	return newQuestion
}

func advanceTurn(roomID uuid.UUID) {
	room := gameCache[roomID]

	currentTurnIndex := 0

	for i, p := range room.Players {
		if room.Turn == p {
			currentTurnIndex = i
			break
		}
	}

	if currentTurnIndex == len(room.Players)-1 {
		currentTurnIndex = 0
	} else {
		currentTurnIndex++
	}

	gameCache[roomID].Turn = gameCache[roomID].Players[currentTurnIndex]
}
