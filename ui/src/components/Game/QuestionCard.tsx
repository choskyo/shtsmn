import React from 'react';
import { Card } from 'react-bootstrap';
import { fetchUser } from '../../api/users';
import { Question } from '../../dummyQuestions';

interface Props {
	question: Question;
	currentResponder: string;
}

interface State {
	cardVariant:
		| 'primary'
		| 'secondary'
		| 'success'
		| 'danger'
		| 'warning'
		| 'info'
		| 'dark'
		| 'light';
	playerName: string;
}

export default class QuestionCard extends React.Component<Props, State> {
	public state: State = {
		cardVariant: 'primary',
		playerName: '',
	};

	public async componentDidMount() {
		let variant:
			| 'primary'
			| 'secondary'
			| 'success'
			| 'danger'
			| 'warning'
			| 'info'
			| 'dark'
			| 'light' = 'light';

		switch (this.props.question.category) {
			case 'personal':
				variant = 'light';
				break;
			case 'risque':
				variant = 'danger';
				break;
			case 'intellectual':
				variant = 'info';
				break;
			case 'controversial':
				variant = 'dark';
				break;
		}

		const player = await fetchUser(this.props.currentResponder);

		this.setState({
			...this.state,
			cardVariant: variant,
			playerName: player.name,
		});
	}

	public async componentDidUpdate(props: Props) {
		if (this.props.currentResponder !== props.currentResponder) {
			const player = await fetchUser(this.props.currentResponder);

			this.setState({
				...this.state,
				playerName: player.name,
			});
		}

		if (this.props.question.category !== props.question.category) {
			let variant:
				| 'primary'
				| 'secondary'
				| 'success'
				| 'danger'
				| 'warning'
				| 'info'
				| 'dark'
				| 'light' = 'light';

			switch (this.props.question.category) {
				case 'personal':
					variant = 'light';
					break;
				case 'risque':
					variant = 'danger';
					break;
				case 'intellectual':
					variant = 'info';
					break;
				case 'controversial':
					variant = 'dark';
					break;
			}

			this.setState({
				...this.state,
				cardVariant: variant,
			});
		}
	}

	public render() {
		return (
			<Card
				bg={this.state.cardVariant}
				text={this.state.cardVariant === 'light' ? 'dark' : 'white'}>
				<Card.Header>{this.props.question.category}</Card.Header>
				<Card.Body>
					<Card.Title>{this.state.playerName}, </Card.Title>
					<Card.Text>{this.props.question.text}</Card.Text>
				</Card.Body>
			</Card>
		);
	}
}
