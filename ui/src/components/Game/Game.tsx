import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';
import { closeRoom } from '../../api/rooms';
import { refreshGameState } from '../../store/gameplay/actions';
import { AppState } from '../../store/store';
import { RoomInProgress } from '../../types/room';
import { MessageTypes, wsConn } from '../../ws';
import Chatbox from './Chatbox';
import PlayerBox from './PlayerBox';
import QuestionCard from './QuestionCard';

interface Props {
	refreshGameState: typeof refreshGameState;
	room: RoomInProgress;
	initialising: boolean;
}

interface RouteProps {
	id: string;
}

class Game extends React.Component<Props & RouteComponentProps<RouteProps>> {
	public async componentDidMount() {
		this.props.refreshGameState(this.props.match.params.id);
	}

	public componentWillUnmount() {
		wsConn.close();
	}

	private closeGame = async () => {
		const ok = await closeRoom(this.props.room.id);

		if (ok) {
			this.props.history.push('/lobby');
		}
	};

	private toggleReady = () => {
		wsConn.send(
			`${MessageTypes.ToggleReady}:${sessionStorage.getItem('token') || ''}:`
		);
	};

	public render() {
		if (this.props.initialising || !this.props.room) {
			return <p>Loading!</p>;
		}

		return (
			<>
				<Row className="align-items-center">
					<Col
						className="d-flex justify-content-center"
						style={{ marginBottom: '40px' }}
						sm={12}
						md={3}>
						<Button
							size="lg"
							variant="outline-warning"
							onClick={() => this.closeGame()}>
							End Game
						</Button>
					</Col>
					<Col style={{ marginBottom: '40px' }} md={6}>
						<QuestionCard
							currentResponder={this.props.room.turn}
							question={
								this.props.room.questions[this.props.room.questions.length - 1]
							}
						/>
					</Col>
					<Col
						style={{ marginBottom: '40px' }}
						className="d-flex justify-content-center"
						md={3}
						sm={12}>
						<Button
							size="lg"
							variant={
								this.props.room.readyPlayers.includes('qwe')
									? 'success'
									: 'outline-success'
							}
							onClick={() => this.toggleReady()}>
							Ready
						</Button>
					</Col>
				</Row>
				<Row>
					<Col style={{ marginBottom: '40px' }} sm={12} md={9}>
						<Chatbox />
					</Col>
					<Col sm={12} md={3}>
						<PlayerBox />
					</Col>
				</Row>
			</>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	room: state.game.room,
	initialising: state.game.initialising,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => {
	return {
		refreshGameState: (id: string) => {
			dispatch(refreshGameState(id));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withRouter(Game as any));
