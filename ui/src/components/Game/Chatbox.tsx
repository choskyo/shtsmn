import React, { ChangeEvent } from 'react';
import { Button, Col, FormControl, InputGroup, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { clearMessages, sendMessage } from '../../store/chat/actions';
import { AppState } from '../../store/store';
import ChatMessage from '../../types/chatMessage';

interface Props {
	history: ChatMessage[];
	sendMessage: typeof sendMessage;
	clearMessages: typeof clearMessages;
}

interface State {
	messageInput: string;
}

class Chatbox extends React.Component<Props, State> {
	state = {
		messageInput: '',
	};

	public componentWillUnmount() {
		this.props.clearMessages();
	}

	private sendMessage = () => {
		this.props.sendMessage(this.state.messageInput);
		this.setState({
			messageInput: '',
		});
	};

	public render() {
		return (
			<>
				<Row>
					<Col xs={2}>Time</Col>
					<Col xs={2}>From</Col>
					<Col xs={8}></Col>
				</Row>
				<hr />
				<Row
					style={{
						height: '400px',
						overflowY: 'auto',
						overflowX: 'hidden',
						flexFlow: 'column',
					}}>
					{this.props.history.map(msg => {
						return (
							<div
								key={`${msg.senderId}${msg.time.format('x')}`}
								className="d-flex">
								<Col xs={2}>{msg.time.format('HH:mm:ss')}</Col>
								<Col xs={2}>{msg.senderName}</Col>
								<Col xs={8}>{msg.content}</Col>
							</div>
						);
					})}
				</Row>
				<br />
				<hr />
				<br />
				<Col xs={12}>
					<Row>
						<InputGroup>
							<FormControl
								value={this.state.messageInput}
								onChange={(e: ChangeEvent<HTMLInputElement>) =>
									this.setState({ ...this.state, messageInput: e.target.value })
								}
								placeholder="Send message..."
							/>
							<InputGroup.Append>
								<Button
									onClick={() => this.sendMessage()}
									variant="outline-secondary">
									Send
								</Button>
							</InputGroup.Append>
						</InputGroup>
					</Row>
				</Col>
			</>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	history: state.chat.history,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => {
	return {
		sendMessage: (msg: string) => {
			dispatch(sendMessage(msg));
		},
		clearMessages: () => {
			dispatch(clearMessages());
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Chatbox as any);
