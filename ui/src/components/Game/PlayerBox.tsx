import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { fetchUser } from '../../api/users';
import { AppState } from '../../store/store';

interface Props {
	players: string[];
	turn: string;
	readyPlayers: string[];
}

interface State {
	players: IdNamePair[];
}

interface IdNamePair {
	id: string;
	name: string;
}

class PlayerBox extends React.Component<Props, State> {
	state = {
		players: [] as IdNamePair[],
	};

	public async componentDidMount() {
		const players: IdNamePair[] = [];

		for (const p of this.props.players) {
			const dets = await fetchUser(p);
			players.push({
				id: dets.id,
				name: dets.name,
			});
		}

		this.setState({ players });
	}

	public async componentDidUpdate(props: Props) {
		if (props.players.length !== this.props.players.length) {
			const players: IdNamePair[] = [];

			for (const p of this.props.players) {
				const dets = await fetchUser(p);
				players.push({
					id: dets.id,
					name: dets.name,
				});
			}

			this.setState({ players });
		}
	}

	public render() {
		return (
			<>
				<Row>
					<Col xs={6}>Name</Col>
					<Col xs={2}>Ready</Col>
					<Col xs={{ offset: 1, span: 3 }}>Turn</Col>
				</Row>
				<hr />
				{this.state.players.map(p => (
					<Row key={p.id}>
						<Col xs={6}>{p.name}</Col>
						<Col xs={2}>
							{this.props.readyPlayers.includes(p.id) ? 'x' : ''}
						</Col>
						<Col xs={{ offset: 1, span: 3 }}>
							{this.props.turn === p.id ? 'x' : ''}
						</Col>
					</Row>
				))}
			</>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	players: state.game.room.players,
	readyPlayers: state.game.room.readyPlayers,
	turn: state.game.room.turn,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => {
	return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerBox as any);
