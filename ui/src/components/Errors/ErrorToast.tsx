import React from 'react';
import Toast from 'react-bootstrap/Toast';
import { removeError } from '../../store/errors/actions';

interface Props {
	error: { id: string; text: string };
	removeError: typeof removeError;
}

export default class ErrorToast extends React.Component<Props> {
	public componentDidMount() {
		setTimeout(() => {
			this.props.removeError(this.props.error.id);
		}, 5000);
	}

	public render() {
		return (
			<Toast onClose={() => this.props.removeError(this.props.error.id)}>
				<Toast.Header>
					<strong className="mr-auto text-danger">Error</strong>
				</Toast.Header>
				<Toast.Body>{this.props.error.text}</Toast.Body>
			</Toast>
		);
	}
}
