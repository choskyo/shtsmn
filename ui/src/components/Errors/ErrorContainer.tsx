import React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { removeError } from '../../store/errors/actions';
import { AppState } from '../../store/store';
import ErrorToast from './ErrorToast';

interface Props {
	errors: { id: string; text: string }[];
	removeError: typeof removeError;
}

class ErrorContainer extends React.Component<Props> {
	public render() {
		return (
			<div
				style={{
					position: 'absolute',
					bottom: 30,
					left: 30,
				}}>
				{this.props.errors.map(err => (
					<ErrorToast
						key={err.id}
						error={err}
						removeError={() => this.props.removeError(err.id)}
					/>
				))}
			</div>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	errors: state.errors.errors,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => {
	return {
		removeError: (errId: string) => {
			dispatch(removeError(errId));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ErrorContainer as any);
