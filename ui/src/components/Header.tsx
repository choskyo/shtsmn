import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { RouteComponentProps, withRouter } from 'react-router-dom';

class Header extends Component<RouteComponentProps> {
	logout() {
		sessionStorage.clear();
		this.props.history.push('/');
	}

	public render() {
		return (
			<Navbar variant="dark" bg="dark">
				<Navbar.Brand onClick={() => this.props.history.push('/lobby')}>
					質問
				</Navbar.Brand>
				<Navbar.Toggle />
				<Navbar.Collapse className="justify-content-end">
					<Navbar.Text onClick={() => this.logout()}>Logout</Navbar.Text>
				</Navbar.Collapse>
			</Navbar>
		);
	}
}

export default withRouter(Header);
