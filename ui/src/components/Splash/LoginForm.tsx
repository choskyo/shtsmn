import React, { ChangeEvent } from 'react';
import { Alert, Button, Col, Form, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';
import { register } from '../../api/users';
import * as authActions from '../../store/auth/actions';
import { AppState } from '../../store/store';

interface Props {
	login: typeof authActions.login;
	authenticated: boolean;
}

interface State {
	username: string;
	password: string;
	successfulRegistration: boolean;
}

class LoginForm extends React.Component<Props & RouteComponentProps, State> {
	public state = {
		username: '',
		password: '',
		successfulRegistration: false,
	};

	private login = async () => {
		this.props.login(this.state.username, this.state.password);
	};

	private register = async () => {
		const newId = await register(this.state.username, this.state.password);

		if (newId) {
			this.setState({ ...this.state, successfulRegistration: true });
		}
	};

	public componentDidUpdate(props: Props) {
		if (sessionStorage.getItem('token')) {
			this.props.history.push('/lobby');
		}
	}

	public render() {
		return (
			<Form>
				<Form.Group controlId="formBasicEmail">
					<Form.Label>Username</Form.Label>
					<Form.Control
						type="text"
						placeholder="Username"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							this.setState({ ...this.state, username: e.target.value })
						}
					/>
				</Form.Group>

				<Form.Group controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							this.setState({ ...this.state, password: e.target.value })
						}
					/>
				</Form.Group>
				<Row>
					<Col xs={12} className="d-flex justify-content-around">
						<Button
							variant="primary"
							type="submit"
							onClick={async (e: React.MouseEvent<HTMLButtonElement>) => {
								e.preventDefault();
								await this.login();
							}}>
							Login
						</Button>
						<Button
							variant="primary"
							type="submit"
							onClick={async (e: React.MouseEvent<HTMLButtonElement>) => {
								e.preventDefault();
								await this.register();
							}}>
							Register
						</Button>
					</Col>
				</Row>
				<br />
				{this.state.successfulRegistration ? (
					<Alert variant={'success'}>
						Registration successful, you can login now.
					</Alert>
				) : (
					<></>
				)}
			</Form>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	authenticated: state.auth.authenticated,
});

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => {
	return {
		login: (name: string, password: string) => {
			dispatch(authActions.login(name, password));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withRouter(LoginForm as any));
