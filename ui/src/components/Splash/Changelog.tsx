import { Moment } from 'moment';
import React from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import changelogs from '../../changelog';

interface State {
	changelogs: { date: Moment; changes: string[] }[];
}

export default class Changelog extends React.Component<{}, State> {
	state = {
		changelogs: [] as { date: Moment; changes: string[] }[],
	};

	public componentDidMount() {
		this.setState({
			changelogs,
		});
	}

	public render() {
		return (
			<Accordion defaultActiveKey="0">
				{this.state.changelogs.map(changelog => (
					<Card>
						<Card.Header>
							<Accordion.Toggle
								as={Button}
								variant="link"
								eventKey={this.state.changelogs.indexOf(changelog).toString()}>
								{changelog.date.format('YYYY/MM/DD')}
							</Accordion.Toggle>
						</Card.Header>
						<Accordion.Collapse
							eventKey={this.state.changelogs.indexOf(changelog).toString()}>
							<Card.Body>
								<ul>
									{changelog.changes.map(change => (
										<li>{change}</li>
									))}
								</ul>
							</Card.Body>
						</Accordion.Collapse>
					</Card>
				))}
			</Accordion>
		);
	}
}
