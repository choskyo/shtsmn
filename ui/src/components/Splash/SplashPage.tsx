import React from 'react';
import { Alert } from 'react-bootstrap';
import Col from 'react-bootstrap/Col';
import Changelog from './Changelog';
import LoginForm from './LoginForm';

export default class SplashPage extends React.Component<{}> {
	public render() {
		return (
			<>
				<Col xs={12} md={{ span: 6, offset: 3 }}>
					<Alert variant="warning">
						This is an early prototype, quirks and problems are to be expected.
					</Alert>
				</Col>
				<Col xs={12} md={{ span: 4, offset: 4 }}>
					<br />
					<LoginForm />
				</Col>
				<Col xs={12} md={{ span: 8, offset: 2 }}>
					<h3>Changelog</h3>
					<Changelog />
				</Col>
			</>
		);
	}
}
