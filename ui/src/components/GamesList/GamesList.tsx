import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { fetchOpenRooms, joinRoom } from '../../api/rooms';
import Room from '../../types/room';
import EnterPasswordModel from './EnterPasswordModel';

interface State {
	rooms: Room[];
	loading: boolean;
	showPasswordPopup: boolean;
	selectedRoom: Room;
}

class GamesList extends Component<RouteComponentProps, State> {
	state = {
		rooms: [] as Room[],
		loading: true,
		showPasswordPopup: false,
		selectedRoom: {} as Room,
	};

	public async componentDidMount() {
		await this.loadRooms();
	}

	private loadRooms = async () => {
		const rooms = await fetchOpenRooms();

		this.setState({
			...this.state,
			loading: false,
			rooms,
		});
	};

	private joinRoom = async (room: Room, password: string = '') => {
		if (room.hasPassword && !password) {
			this.setState({
				...this.state,
				showPasswordPopup: true,
				selectedRoom: room,
			});
			return;
		}

		const ok = await joinRoom(room.id, password);

		if (ok) {
			this.props.history.push(`/rooms/${room.id}`);
		}
	};

	private hide = () => {
		this.setState({
			...this.state,
			showPasswordPopup: false,
		});
	};

	public render() {
		if (this.state.loading) {
			return <h3 className="text-center">Fetching games...</h3>;
		}

		if (this.state.rooms.length === 0) {
			return <h3 className="text-center">0 rooms</h3>;
		}

		return (
			<>
				<Row>
					<Col xs={6}>Name</Col>
					<Col className="text-center" xs={2}>
						Players
					</Col>
					<Col className="text-center" xs={2}>
						Password
					</Col>
					<Col xs={2}>
						<Button size="sm" onClick={() => this.loadRooms()} variant="link">
							Refresh
						</Button>
					</Col>
				</Row>
				<hr />
				{this.state.rooms.map(room => (
					<Row key={room.id} className="d-flex align-items-center">
						<Col xs={6}>{room.name}</Col>
						<Col className="text-center" xs={2}>
							{room.players.length}
						</Col>
						<Col className="text-center" xs={2}>
							{room.hasPassword ? 'X' : ''}
						</Col>
						<Col className="text-center" xs={2}>
							<Button onClick={() => this.joinRoom(room)} variant="link">
								Join
							</Button>
						</Col>
					</Row>
				))}
				<EnterPasswordModel
					room={this.state.selectedRoom}
					show={this.state.showPasswordPopup}
					joinRoom={this.joinRoom}
					hide={this.hide}
				/>
			</>
		);
	}
}

export default withRouter(GamesList as any);
