import React, { ChangeEvent } from 'react';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import Modal from 'react-bootstrap/Modal';
import Room from '../../types/room';

interface Props {
	room: Room;
	show: boolean;
	joinRoom: (r: Room, p: string) => void;
	hide: () => void;
}

interface State {
	password: string;
}

export default class EnterPasswordModel extends React.Component<Props, State> {
	state = {
		password: '',
	};

	public render() {
		return (
			<Modal
				onHide={() => this.props.hide()}
				show={this.props.show}
				size="lg"
				aria-labelledby="contained-modal-title-vcenter"
				centered>
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title-vcenter">
						Password required to join
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<FormControl
						placeholder="Password"
						aria-label="Password"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							this.setState({ ...this.state, password: e.target.value })
						}
					/>
				</Modal.Body>
				<Modal.Footer>
					<Button
						variant="outline-primary"
						onClick={() => {
							this.props.hide();
							this.setState({ password: '' });
						}}>
						Cancel
					</Button>
					<Button
						variant="success"
						onClick={() => {
							this.props.joinRoom(this.props.room, this.state.password);
							this.setState({ password: '' });
						}}>
						Join
					</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}
