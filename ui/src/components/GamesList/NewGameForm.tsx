import React, { ChangeEvent } from 'react';
import { InputGroup } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { createRoom } from '../../api/rooms';

interface Props {
	// loadRooms: () => Promise<void>;
}

interface State {
	name: string;
	password: string;
}

export default class NewGameForm extends React.Component<Props, State> {
	state = {
		name: '',
		password: '',
	};

	public render() {
		return (
			<InputGroup className="mb-3">
				<Form.Control
					type="text"
					placeholder="Room name"
					value={this.state.name}
					onChange={(e: ChangeEvent<HTMLInputElement>) =>
						this.setState({ ...this.state, name: e.target.value })
					}
				/>
				<Form.Control
					type="text"
					placeholder="Password (optional)"
					value={this.state.password}
					onChange={(e: ChangeEvent<HTMLInputElement>) =>
						this.setState({ ...this.state, password: e.target.value })
					}
				/>
				<InputGroup.Append>
					<Button
						variant="success"
						type="submit"
						onClick={async (e: React.MouseEvent<HTMLButtonElement>) => {
							e.preventDefault();
							await createRoom(this.state.name, this.state.password);
							this.setState({ name: '', password: '' });
							// await this.props.loadRooms();
						}}>
						Create
					</Button>
				</InputGroup.Append>
			</InputGroup>
		);
	}
}
