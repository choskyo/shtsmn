import moment, { Moment } from 'moment';

const changelogs: { date: Moment; changes: string[] }[] = [
	{
		date: moment('2020/04/13'),
		changes: [
			'Added changelogs.',
			'Added ability for room creator to close room.',
		],
	},
	{
		date: moment('2020/04/12'),
		changes: [
			'Redirect to login page when auth fails.',
			"Fixed close button on the 'joining a room' password input popup.",
		],
	},
];

export default changelogs;
