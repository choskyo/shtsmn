import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import { Route, Switch } from 'react-router-dom';
import ErrorContainer from './components/Errors/ErrorContainer';
import Chatbox from './components/Game/Chatbox';
import Game from './components/Game/Game';
import GamesList from './components/GamesList/GamesList';
import NewGameForm from './components/GamesList/NewGameForm';
import Header from './components/Header';
import SplashPage from './components/Splash/SplashPage';
import CustomRouter, { history } from './CustomRouter';

function App() {
	return (
		<CustomRouter history={history}>
			<ErrorContainer />
			<Header />
			<br />
			<br />
			<Container>
				<Switch>
					<Route exact path="/">
						<SplashPage />
					</Route>
					<Route exact path="/lobby">
						<Row>
							<Col xs={12} md={6} style={{ marginBottom: '30px' }}>
								<h3>Rooms</h3>
								<hr />
								{/* <Col xs={12}> */}
								<NewGameForm />
								{/* </Col> */}
								<GamesList />
							</Col>
							<Col xs={12} md={6}>
								<h3>Chatbox</h3>
								<hr />
								<Chatbox />
							</Col>
						</Row>
					</Route>
					<Route exact path="/rooms/:id">
						<Game />
					</Route>
				</Switch>
			</Container>
		</CustomRouter>
	);
}

export default App;
