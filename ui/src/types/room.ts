import { Question } from '../dummyQuestions';
import ChatMessage from './chatMessage';
import { User } from './user';

export default interface Room {
	id: string;
	name: string;
	creatorId: string;
	creator: string;
	players: User[];
	activeQuestion: Question;
	hasPassword: boolean;
}

export interface RoomInProgress {
	id: string;
	name: string;
	creatorId: string;
	players: string[]; // uuid[]
	readyPlayers: string[]; // uuid[]
	questions: Question[];
	hasPassword: boolean;
	messages: ChatMessage[];
	goodAnswers: {};
	turn: string;
}
