export interface User {
	id: string;
	name: string;
	ready: boolean;
}
