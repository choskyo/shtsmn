import moment from 'moment';

export default interface ChatMessage {
	senderId: string;
	senderName: string;
	time: moment.Moment;
	content: string;
}
