export default interface Card {
	revealed: boolean;
	text: string;
	category: string;
}
