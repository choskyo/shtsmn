import Room, { RoomInProgress } from '../types/room';
import { apiRequest } from './requests';

export const fetchOpenRooms = async (): Promise<Room[]> => {
	const res = await apiRequest('rooms', 'v1', 'list', 'GET');

	return res.ok ? res.json() : [];
};

export const getRoomDetails = async (id: string): Promise<RoomInProgress> => {
	const res = await apiRequest('rooms', 'v1', `details/${id}`, 'GET');

	return res.ok ? res.json() : {};
};

export const joinRoom = async (
	id: string,
	password: string
): Promise<boolean> => {
	const res = await apiRequest('rooms', 'v1', `join/${id}`, 'POST', {
		id,
		password,
	});

	return res.ok;
};

export const createRoom = async (
	name: string,
	password: string
): Promise<boolean> => {
	const res = await apiRequest('rooms', 'v1', 'create', 'POST', {
		name,
		password,
	});

	return res.ok;
};

export const closeRoom = async (id: string): Promise<boolean> => {
	const res = await apiRequest('rooms', 'v1', `close/${id}`, 'GET');

	return res.ok;
};
