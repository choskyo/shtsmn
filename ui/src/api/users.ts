import LoginResponse from '../types/loginResponse';
import { User } from '../types/user';
import { apiRequest } from './requests';

export const fetchUser = async (id: string): Promise<User> => {
	const res = await apiRequest('users', 'v1', `fetch/${id}`, 'GET');

	return res.ok ? ((await res.json()).user as User) : ({} as User);
};

export const register = async (
	name: string,
	password: string
): Promise<string> => {
	const res = await apiRequest(
		'users',
		'v1',
		'register',
		'POST',
		{ name, password },
		false
	);

	return res.ok ? (await res.json()).id : '';
};

export const login = async (
	name: string,
	password: string
): Promise<boolean> => {
	const res = await apiRequest(
		'users',
		'v1',
		'login',
		'POST',
		{ name, password },
		false
	);

	if (!res.ok) {
		return false;
	}

	const json = (await res.json()) as LoginResponse;

	sessionStorage.setItem('token', json.token);

	setInterval(async () => {
		await renew();
	}, 1800000);

	return true;
};

const renew = async (): Promise<void> => {
	const res = await apiRequest('users', 'v1', 'renew', 'GET');

	const json = (await res.json()) as LoginResponse;

	sessionStorage.setItem('token', json.token);
};
