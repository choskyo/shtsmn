import { User } from '../../types/user';

export const SET_USER = 'SET_USER';
export const LOGOUT = 'LOGOUT';

export interface AuthState {
	user: User;
	authenticated: boolean;
}

interface Login {
	type: typeof SET_USER;
	user: User;
}

interface Logout {
	type: typeof LOGOUT;
}

export type AuthActions = Login | Logout;
