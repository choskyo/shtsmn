import { User } from '../../types/user';
import { AuthActions, AuthState, LOGOUT, SET_USER } from './types';

const initialState: AuthState = {
	user: {} as User,
	authenticated: false,
};

export const authReducer = (
	state = initialState,
	action: AuthActions
): AuthState => {
	switch (action.type) {
		case SET_USER: {
			return { ...state, authenticated: true };
		}
		case LOGOUT: {
			return { ...state, authenticated: false };
		}
		default: {
			return state;
		}
	}
};
