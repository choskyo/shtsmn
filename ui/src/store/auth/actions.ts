import * as authAPI from '../../api/users';
import { SET_USER } from './types';

export const login = (name: string, password: string) => {
	return async (dispatch: any) => {
		await authAPI.login(name, password);

		dispatch(setActiveUser());
	};
};

const setActiveUser = () => {
	return {
		type: SET_USER,
	};
};
