import { RoomInProgress } from '../../types/room';
import { GameplayAction, GameplayState, SET_ROOM } from './types';

const initialState: GameplayState = {
	room: {} as RoomInProgress,
	initialising: true,
};

export const gameplayReducer = (
	state = initialState,
	action: GameplayAction
): GameplayState => {
	switch (action.type) {
		case SET_ROOM: {
			return {
				...state,
				room: action.state,
				initialising: false,
			};
		}
		default: {
			return state;
		}
	}
};
