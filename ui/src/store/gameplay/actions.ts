import { apiRequest } from '../../api/requests';
import { RoomInProgress } from '../../types/room';
import { GameplayAction, SET_ROOM } from './types';

export const refreshGameState = (id: string) => {
	return async (dispatch: any) => {
		try {
			const res = await apiRequest('rooms', 'v1', `details/${id}`, 'GET');

			const body = (await res.json()).room as RoomInProgress;

			dispatch(setGameState(body));
		} catch (err) {
			console.error(err);
		}
	};
};

export const setGameState = (state: RoomInProgress): GameplayAction => {
	return {
		type: SET_ROOM,
		state,
	};
};
