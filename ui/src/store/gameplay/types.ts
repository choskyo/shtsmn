import { RoomInProgress } from '../../types/room';

export const UPDATE_GAME = 'UPDATE';
export const SET_ROOM = 'SET_ROOM';

interface SetRoomState {
	type: typeof SET_ROOM;
	state: RoomInProgress;
}

export type GameplayAction = SetRoomState;

export interface GameplayState {
	room: RoomInProgress;
	initialising: boolean;
}
