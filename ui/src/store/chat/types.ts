import ChatMessage from '../../types/chatMessage';

export const SEND_MESSAGE = 'SENDMESSAGE';
export const RECEIVE_MESSAGE = 'RECEIVEMESSAGE';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';

interface SendMessage {
	type: typeof SEND_MESSAGE;
}

interface ReceiveMessage {
	type: typeof RECEIVE_MESSAGE;
	message: ChatMessage;
}

interface ClearMessages {
	type: typeof CLEAR_MESSAGES;
}

export type ChatAction = SendMessage | ReceiveMessage | ClearMessages;

export interface ChatState {
	history: ChatMessage[];
}
