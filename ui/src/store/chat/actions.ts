import ChatMessage from '../../types/chatMessage';
import { MessageTypes, wsConn } from '../../ws';
import {
	ChatAction,
	CLEAR_MESSAGES,
	RECEIVE_MESSAGE,
	SEND_MESSAGE,
} from './types';

export const sendMessage = (message: string) => {
	if (!wsConn) {
		console.error('null wsConn');
		return {
			type: SEND_MESSAGE,
		};
	}
	wsConn.send(
		`${MessageTypes.Chat}:${sessionStorage.getItem('token') || ''}:${message}`
	);

	return {
		type: SEND_MESSAGE,
	};
};

export const receiveMessage = (message: ChatMessage): ChatAction => {
	return {
		type: RECEIVE_MESSAGE,
		message,
	};
};

export const clearMessages = (): ChatAction => {
	return {
		type: CLEAR_MESSAGES,
	};
};
