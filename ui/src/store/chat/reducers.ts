import ChatMessage from '../../types/chatMessage';
import {
	ChatAction,
	ChatState,
	CLEAR_MESSAGES,
	RECEIVE_MESSAGE,
	SEND_MESSAGE,
} from './types';

const initialState: ChatState = {
	history: [] as ChatMessage[],
};

export const chatReducer = (
	state = initialState,
	action: ChatAction
): ChatState => {
	switch (action.type) {
		case SEND_MESSAGE: {
			return { ...state };
		}
		case RECEIVE_MESSAGE: {
			return {
				...state,
				history: [...state.history, action.message],
			};
		}
		case CLEAR_MESSAGES: {
			return {
				...state,
				history: [],
			};
		}
		default: {
			return state;
		}
	}
};
