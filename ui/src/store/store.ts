import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from './auth/reducers';
import { chatReducer } from './chat/reducers';
import { errorReducer } from './errors/reducers';
import { gameplayReducer } from './gameplay/reducers';

const rootReducer = combineReducers({
	auth: authReducer,
	chat: chatReducer,
	game: gameplayReducer,
	errors: errorReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
