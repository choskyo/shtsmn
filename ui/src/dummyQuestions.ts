export interface Question {
  id: string;
  category: "personal" | "risque" | "controversial" | "intellectual";
  text: string;
}

// export const dummyQuestions: Question[] = [
// ];
