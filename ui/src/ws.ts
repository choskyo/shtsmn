import moment from "moment";
import { webRoot } from "./api/requests";
import { history } from "./CustomRouter";
import { receiveMessage } from "./store/chat/actions";
import { addError } from "./store/errors/actions";
import { setGameState } from "./store/gameplay/actions";
import store from "./store/store";
import ChatMessage from "./types/chatMessage";
import { RoomInProgress } from "./types/room";

export let wsConn: WebSocket;

export enum MessageTypes {
  Chat = "chat",
  Init = "init",
  ToggleReady = "toggleReady",
}

export const initialiseWs = () => {
  wsConn = new WebSocket(`ws://${webRoot.split("://")[1]}/ws`);

  wsConn.onopen = () => {
    wsConn.send(
      `${MessageTypes.Init}:${sessionStorage.getItem("token") as string}:`
    );
  };

  wsConn.onmessage = (msg: MessageEvent) => {
    const data = msg.data as string;
    const messageType = data.substring(0, data.indexOf(":"));
    const messageContents = data.substring(data.indexOf(":") + 1);

    // console.log(messageContents);

    switch (messageType) {
      case "chat": {
        try {
          const chatMessage = JSON.parse(messageContents) as ChatMessage;
          chatMessage.time = moment();
          if (!chatMessage.content) {
            throw new Error("invalid message received");
          }
          store.dispatch(receiveMessage(chatMessage));
        } catch (err) {
          store.dispatch(addError(err.message));
        }
        break;
      }
      case "gameUpdate": {
        try {
          const room = JSON.parse(messageContents) as RoomInProgress;
          store.dispatch(setGameState(room));
        } catch (err) {
          store.dispatch(addError(err.message));
        }
        break;
      }
      case "roomClosed": {
        store.dispatch(addError("Room has been closed by the host."));
        history.push("/lobby");
      }
    }
  };
};
